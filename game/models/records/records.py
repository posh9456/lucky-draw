from django.db import models
class Record(models.Model):
    player_name = models.CharField(max_length=50)  # 人名
    prize_name = models.CharField(max_length=50)   # 奖品名
    time = models.DateTimeField(auto_now_add = True) #中奖时间
        
    def __str__(self):
        return str(self.player_name)
        
