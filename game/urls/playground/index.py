from django.urls import path, include
from game.views.playground.index import get_prizes, get_players, rec_records

urlpatterns = [
        path("prizes/", get_prizes, name="playground_get_prizes"),
        path("players/", get_players, name="playground_get_players"),
        path("records/", rec_records, name="playground_rec_records"),
]
