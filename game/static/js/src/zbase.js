class Lottery{ //类
	constructor(id){ //构造函数。一定要传入一个参数。在这里就是"ac_game_12345678"
		this.id = id;
		this.$lottery_game = $('#' + id); 
		this.menu = new LotteryMenu(this); //创建菜单对象
		this.playground = new LotteryGame(this);//创建游戏界面对象
		//this.settings = new Settings(this);//创建设置对象

		this.start();  //创建对象之后自动调用start成员函数
	}

	start(){  //这里没有东西
	}
}
