# LuckyDraw
东北大学2023-2024春软件工程抽奖系统

## 环境
- Ubuntu 20.04
- Django 3.2.8
- Python 3.8.10

## 配置
在luckyDraw文件夹下的settings.py中，在ALLOWED_HOSTS列表里添加自己的服务器地址

## 使用
在终端输入python3 manage.py runserver 0.0.0.0:8000，然后在浏览器中输入服务器地址（或者是本机地址）的8000端口。