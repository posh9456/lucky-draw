from django.contrib import admin
from game.models.players.players import Player
from game.models.prizes.prizes import Prize
from game.models.records.records import Record

# Register your models here.

admin.site.register(Player)
admin.site.register(Prize)

class RecordAdmin(admin.ModelAdmin):
        readonly_fields = ('player_name', 'prize_name', 'time')

admin.site.register(Record, RecordAdmin)

