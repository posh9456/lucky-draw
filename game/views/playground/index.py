from django.http import JsonResponse
from game.models.prizes.prizes import Prize
from game.models.players.players import Player
from game.models.records.records import Record
def get_prizes(request):
    prize = Prize.objects.all();
    names = []
    numbers = []

    for i in range(len(prize)):
        names.append(prize[i].name)
        numbers.append(prize[i].number)

    return JsonResponse({
	'result': "success", 
	'prize_name': names, 
	'prize_number': numbers, 
	})

def get_players(request):
    player = Player.objects.all();

    names = []
    phones = []

    for i in range(len(player)):
        names.append(player[i].name)
        phones.append(player[i].phone_number)

    return JsonResponse({ # 返回Json
	'result': "success", # 表明查询成功
	'player_name': names, # 用户名
	'phone': phones, # 头像URL
	})

def rec_records(request):
    data = request.GET

    player = data.get("player")
    prize = data.get("prize")

    if player is None or prize is None:
        return JsonResponse({'result': "failed",})

    Record.objects.create(player_name = player, prize_name = prize, time = None)
    return JsonResponse({'result': "success",})
    
