class LotteryGame{
	constructor(root){//构造函数，传入总对象（在这里是AcGame）
		this.root = root;
		this.$playground = $(`
<div class="lottery-playground">
    <div class="lottery-playground-field">

		<div class="lottery-playground-field-item lottery-playground-field-item-select">
			<li><a href="#" id="bonus">SELECT THE PRIZE</a>
				<ul id="options">
				</ul>
			</li>

		</div>

		<div>
		<button id="start" class="lottery-playground-field-butt lottery-playground-field-item-start">**START**</button>
	</div>

	<div> 
		<button id="restart" class="lottery-playground-field-butt lottery-playground-field-item-restart">**RESTART**</button>
	</div>

	<div class="lottery-playground-field-item lottery-playground-field-item-show" id="roll">
		谁会中奖？
	</div>

    </div>
</div>
`);
		this.root.$lottery_game.append(this.$playground); 
		this.$start_button = $("#start");
		this.$restart_button = $("#restart");
		this.$roll = $("#roll");
		//this.get_players = 0; //已经拿到用户的名单 outer.player和outer.phone
		this.fin = 0;
		this.time = 5;
		this.curprize = "NULL";
		this.start();
		//this.prize是奖品的列表；this.num是各色奖品的个数列表；this.curprize是当前选中的奖品
		//this.player是人的列表；this.phone是各色人的电话列表
	}

	start(){
		this.hide();
		this.add_prizes(); 
		this.get_players();
		this.add_listening_events();  //添加监听事件
	}

	send_record(){

		let outer = this;

		$.ajax({
			url: "http://39.105.158.56:8000/playground/records", // 访问url
			type: "GET",
			data: {
				player: outer.lucky_man, // 传输数据
				prize: outer.curprize, 
			},
			success: function(resp){
				console.log(resp); // 测试输出
				if (resp.result == "success"){
					console.log("SUCEED IN SENDING REOCRD!");
				}
				else{
					console.log("FAILED IN SENDING REOCRD!");
				}
			}
		});
	}

	add_listening_events(){
		let outer = this;

		outer.$start_button.click(function(){
			console.log("你好");
			if(outer.fin == 0){  //还没有开始

				if(outer.curprize == "NULL"){
					outer.$roll.text("请先选择奖品！");
					return;
				}

				for(var i = 0; i < outer.prize.length; i++){
					if(outer.prize[i] == outer.curprize){
						if(outer.num[i] == 0){
							outer.$roll.text("该奖品已经没有了！");
							return;
						}
						outer.num[i]--;	//把当前的奖品数量减一
					}
				}

				outer.$start_button.text("**ROLLING!!**");
				outer.fin = 1; //正在抽奖。此时START按钮无效
				var x = outer.player.length - 1;
				var y = 0;
				var rand = parseInt(Math.random() * (x - y + 1) + y);
				outer.lucky_man = outer.player[rand]; //随机选一个人

				countdown(outer.$roll, outer.lucky_man, 5);

				//添加抽奖记录
				outer.send_record()
				outer.fin = 2;
			}
			else if(outer.fin == 2){ //抽奖已经结束
				outer.$start_button.text("**START**");
				outer.fin = 0;
			}
			else{}
		});

		outer.$restart_button.click(function(){
			outer.$roll.text("谁会中奖？");
			outer.fin = 0;

			$("#bonus").text("SELECT THE PRIZE");
			for(var i = 0; i < outer.num_copy.length; i++){
				outer.num[i] = outer.num_copy[i];
			}
			outer.curprize = "NULL";
			outer.$start_button.text("**START**");
		});
	}

	select_prizes() {
		let outer = this;
	/*	$(".lottery-playground-field-item-select>li").hover( //鼠标悬浮上去，就展开选项
			function(){
				$(this).children("ul").slideDown();
			},
			function(){
				$(this).children("ul").slideUp();
			}
		);*/

		$(".lottery-playground-field-item-select>li>ul>li").click(	//点击就改变
			function(){
				$("#bonus").text(this.innerText);
				outer.curprize = this.innerText;		//确定当前选定的奖品
			}
		);
	}

	add_prizes() 
	{
		let outer = this;

		$.ajax({ 
			url: "http://39.105.158.56:8000/playground/prizes", // 获取信息的URL
			type: "GET",
			success: function(resp){
				console.log(resp); 
				if (resp.result === "success")
				{

					outer.prize = resp.prize_name;
					outer.num = new Array();
					outer.num_copy = new Array();	//存一个副本，好在重置的时候用
					console.log(resp.prize_number);
					let list = $("#options");

					for(var i = 0; i < outer.prize.length; i++){
						let str = '<li><a href="#">' + outer.prize[i] + '</a></li>';
						list.append($(str));

						outer.num[i] = resp.prize_number[i];
						outer.num_copy[i] = outer.num[i];
					}
					outer.select_prizes();
				}
				else
				{ 
					console.log("Failed to get prizes!!!");
				}
			}
		})
	}

	get_players() 
	{
		let outer = this;
		$.ajax({ 
			url: "http://39.105.158.56:8000/playground/players", // 获取信息的URL
			type: "GET",
			success: function(resp){
				console.log(resp); 
				if (resp.result === "success")
				{
					outer.player = resp.player_name;
					console.log(outer.player);
					outer.phone = resp.phone;
				}
				else
				{ 
					console.log("Failed to get players!!!");
				}
			}
		})
	}

	show() { // 打开目录
		this.$playground.show();
	}
	hide() {  //关闭目录
		this.$playground.hide();
	}

}

function countdown(obj, man, time){
	if(time == 0){
		obj.text(man);
		return;
	}
	else{
		obj.text(time);
		time--;
	}
	setTimeout(function(){
		countdown(obj, man, time)
	},1000)
}
