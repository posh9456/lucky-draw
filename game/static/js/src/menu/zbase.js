class LotteryMenu{
	constructor(root){
		this.root = root;
		this.$menu = $(`
<div class="lottery-menu">
    <div class="lottery-menu-field">
	<div class="lottery-menu-field-item lottery-menu-field-item-play_mode">
	    PLAY
	</div>
	<br>
	<div class="lottery-menu-field-item lottery-menu-field-item-settings">
	<a href="admin/" class="lottery-settings">
	    SETTINGS
	</a>
	</div>
    </div>
</div>
`);
		this.root.$lottery_game.append(this.$menu); 
		this.$play_mode = this.$menu.find('.lottery-menu-field-item-play_mode');
		this.$settings = this.$menu.find('.lottery-menu-field-item-settings');
		this.start();
	}

	start() {
		this.add_listening_events(); 
	}

	add_listening_events() {
		let outer = this;
		this.$play_mode.click(function(){ 
			outer.hide(); 
			outer.root.playground.show(); 
		});
		this.$settings.click(function(){
			outer.hide(); 
			outer.root.settings.show();
		});
	}
	show() { 
		this.$menu.show();
	}
	hide() {  
		this.$menu.hide();
	}
}
